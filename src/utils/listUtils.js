var listUtils = {

    getListValues: function (listTag){
        var htmlList = $(listTag),
            list = []

        $.each(htmlList, function (index, value) {
            list.push(value.innerText)
        })

        return list
    },

    applySort: function (list, listTag, evalFunc, attrToSet){
        var htmlList = $(listTag)
        $.each(htmlList, function (index, value) {
            var itemValue = list[index]
            value.innerText = itemValue
            value[attrToSet] = evalFunc(itemValue)
        })
    }
}




