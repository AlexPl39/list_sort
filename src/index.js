$( document ).ready(function() {

    $("#order-list-button").click(function () {
        var unorderedList = listUtils.getListValues("#number-list li").sort(sortUtils.sortDescNumberList)

        listUtils.applySort(unorderedList, "#number-list li", evalUtils.evaluateNumber, "className")
    })
});